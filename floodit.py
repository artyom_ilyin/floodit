from Cluster import Cluster


class Game:

    def start_the_game(self):
        self.cluster.next_turn()

    def __init__(self, dimension, colors):
        self.cluster = Cluster(dimension, colors)


DIMENSION = 8

COLORS = [
    1, 2, 3, 4, 5, 6,
]

game = Game(DIMENSION, COLORS)
game.start_the_game()



