from random import randint


class Cluster:

    directions = {
        'right': {'x': 0, 'y': 1}, # right
        'down': {'x': 1, 'y': 0}, # down
        'left': {'x': 0, 'y': -1}, # left
        'up': {'x': -1, 'y': 0}, # up
    }

    def print(self):
        return print(), [print(self.field[i]) for i in range(self.dim)], print()

    def check_all_cells_the_same(self):
        for row in self.field:
            for cell in row:
                if self.field[0][0] != cell:
                    return False
        return True

    def next_turn(self):

        self.print()
        while not self.check_all_cells_the_same():
            color_changed = False
            while not color_changed:

                new_color = None
                while new_color is None:
                    try:
                        new_color = int(input("(" + str(self.moves) + " moves) pick color: "))
                    except ValueError:
                        pass

                if self.field[0][0] == new_color:
                    print("you should change color")
                else:
                    color_changed = True

                    for row_index, row in enumerate(self.matrix):
                        for cell_index, cell in enumerate(row):
                            if self.matrix[row_index][cell_index] == 1:
                                self.field[row_index][cell_index] = new_color
                                self.matrix[row_index][cell_index] = 0
                    self.expand(new_color=new_color)
                    self.moves += 1
            self.print()

        print("You won! Your score is", self.moves, ".")
        exit(0)

    def expand(self, x=0, y=0, new_color=None):

        for key, direction_val in self.directions.items():
            new_x = x + direction_val['x']
            new_y = y + direction_val['y']
            try:
                if new_x < 0 or new_y < 0:
                    raise IndexError
                if self.field[new_x][new_y] == new_color or self.matrix[new_x][new_y] == 1:
                    if self.matrix[new_x][new_y] == 0:
                        self.matrix[new_x][new_y] = 1
                        self.expand(x=new_x, y=new_y, new_color=new_color)
            except IndexError:
                pass

    def get_random_field(self, dim, colors):

        self.field = [[randint(1, len(colors)) for i in range(dim)] for y in range(dim)]
        if self.check_all_cells_the_same():
            self.get_random_field(dim, colors)
        else:
            return

    def __init__(self, dim, colors):
        self.dim = dim
        self.moves = 0
        self.field = None
        self.winning_matrix = [[0 for i in range(self.dim)] for y in range(self.dim)]
        self.matrix = [[0 for i in range(self.dim)] for y in range(self.dim)]
        self.matrix[0][0] = 1
        self.get_random_field(self.dim, colors)
        self.expand(new_color=self.field[0][0])
